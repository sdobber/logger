﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace LoggerClasses
{
    public class RegistryLog : ILogger
    {
        public void Log(Exception ex)
        {
            string keyName = DateTime.Now.ToString();
            RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\LoggingApplication");
            key.SetValue(keyName, ex.Message);
            key.Close();
        }
    }
}
