﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerClasses
{
    public class FileLog : ILogger
    {
        public void Log(Exception ex)
        {
            string filePath = "C:\\tmp\\log.txt";
            using(StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine(DateTime.Now.ToString() + " " + ex.Message);
                writer.Close();
            }

        }
            
    }
}
