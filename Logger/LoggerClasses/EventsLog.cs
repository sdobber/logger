﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerClasses
{
    public class EventsLog : ILogger
    {
        public void Log(Exception ex)
        {
            using(EventLog log = new EventLog("Application"))
            {
                log.Source = "Application";
                log.WriteEntry(ex.Message);
            }
        }
    }
}
