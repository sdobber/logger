﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Diagnostics;
using System.IO;

namespace UsingLogger
{
    class Program
    {
        static void Main(string[] args)
        {
            ChooseLogger logger1 = new ChooseLogger();
            int[] array = { 1, 2, 3 };
            int a = 0;
            string name = null;
            try
            {
                array[3] = 7;
            }
            catch (Exception ex)
            {
                logger1.ChoosingLogger(0, ex);
            }
            try
            {
                int b = 7 / a;
            }
            catch (Exception ex)
            {
                logger1.ChoosingLogger(1, ex);
            }
            try
            {
                if(name.Length == 0)
                {
                    Console.WriteLine();
                }
            }
            catch (Exception ex)
            {
                logger1.ChoosingLogger(2, ex);
            }
            finally
            {
                string filePath = "C:\\tmp\\log.txt";
                Console.WriteLine(File.ReadAllText(filePath));
                Console.WriteLine();
                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\LoggingApplication");
                if(key != null)
                {
                    foreach (var item in key.GetValueNames())
                    {
                        Console.WriteLine($"{item} {key.GetValue(item)}");
                    }

                }
                key.Close();
                Console.WriteLine();
                EventLog eventLog = new EventLog("Application");
                foreach (EventLogEntry item in eventLog.Entries)
                {
                    if(item.Source == "Application")
                    {
                        Console.WriteLine($"{item.Message}");
                    }
                }
                Console.ReadKey();
            }
        }
    }
}
