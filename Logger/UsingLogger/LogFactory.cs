﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggerClasses;

namespace UsingLogger
{
    public class LogFactory
    {
        public ILogger CreateLogger(int option)
        {
            ILogger logger = null;
            switch (option)
            {
                case 0:
                    logger = new FileLog();
                    break;
                case 1:
                    logger = new RegistryLog();
                    break;
                case 2:
                    logger = new EventsLog();
                    break;
                default:
                    break;
            }
            return logger;
        }
    }
}
