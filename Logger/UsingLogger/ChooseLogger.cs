﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggerClasses;
namespace UsingLogger
{
    public class ChooseLogger
    {
        ILogger logger = null;
        public void ChoosingLogger(int option, Exception ex)
        {
            LogFactory factory = new LogFactory();
            this.logger = factory.CreateLogger(option);
            this.logger.Log(ex);
        }
    }
}
